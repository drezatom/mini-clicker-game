import pygame

from typing import Tuple
from dataclasses import dataclass

from screen import screen
from surface import Surface, Point


@dataclass
class Image:
    path: str
    size: Tuple[int, int]  # in pixel
    surface: Surface

    def load(self):
        return pygame.image.load(self.path)

    def add_to_display(self):
        screen.blit(self.load(), (self.surface.points_top_left.x, self.surface.points_top_left.y))


coins_image = Image(
    path="image\\png\\little.png",
    size=(200, 200),
    surface=Surface(
        points_top_left=Point(x=200, y=300),
        points_bottom_right=Point(x=400, y=500),
    )
)
