import sys
import pygame

from screen import screen
from surface import Point
from image.image import coins_image


class ClickDisplay:

    BACKGROUND_COLOR = (137, 196, 244)

    def __init__(self):
        self.gold = 0

    def screen_update(self):
        screen.fill(self.BACKGROUND_COLOR)
        coins_image.add_to_display()

        font = pygame.font.SysFont("monospace", 16)
        scoretext = font.render("Gold = " + str(self.gold), 1, (0, 0, 0))
        screen.blit(scoretext, (5, 10))

    def check_event(self):
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONUP:
                click_position = Point.from_tuple(event.dict["pos"])
                if coins_image.surface.is_in_surface(click_position):
                    self.gold += 1
            elif event.type == pygame.QUIT:
                sys.exit()

    def run(self):
        while True:
            pygame.display.flip()
            self.screen_update()

            self.check_event()
