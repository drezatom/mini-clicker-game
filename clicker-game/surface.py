from __future__ import annotations

from typing import Tuple
from dataclasses import dataclass


@dataclass
class Point:
    x: int
    y: int

    @staticmethod
    def to_tuple(self) -> Tuple[int, int]:
        return self.x, self.y

    @classmethod
    def from_tuple(cls, coordinate: Tuple[int, int]) -> Point:
        return Point(coordinate[0], coordinate[1])


@dataclass
class Surface:
    points_top_left: Point
    points_bottom_right: Point

    def is_in_surface(self, random_point: Point) -> bool:
        if self.points_top_left.x <= random_point.x <= self.points_bottom_right.x:
            if self.points_top_left.y <= random_point.y <= self.points_bottom_right.y:
                return True
        return False
