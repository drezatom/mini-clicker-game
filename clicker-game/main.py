import pygame

from click_display import ClickDisplay

TITLE_GAME = "Coins game"


if __name__ == '__main__':
    pygame.init()
    pygame.display.set_caption(TITLE_GAME)

    game = ClickDisplay()
    game.run()
